#ifndef DOMBASE_H
#define DOMBASE_H
#include <QString>
#include <QSqlQueryModel>

/**
 * @brief The DomBase class
 * Le but de cette classe est de centraliser les
 * requêtes SQL afin d'éviter la redondance et le
 * copy paste dans le code
 */


class DomBase
{
public:
    virtual QString getTableName() const = 0;
    int getCount() const;
    QSqlQueryModel* selectAll() const;
};

#endif // DOMBASE_H
