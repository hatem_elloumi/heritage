#ifndef EMPLOYEE_H
#define EMPLOYEE_H
#include "dombase.h"

class Employee : public DomBase
{
public:
    QString getTableName() const;

};

#endif // EMPLOYEE_H
