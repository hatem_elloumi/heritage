#ifndef PRODUCT_H
#define PRODUCT_H
#include "dombase.h"

class Product : public DomBase
{
public:
     QString getTableName() const;
};

#endif // PRODUCT_H
