#-------------------------------------------------
#
# Project created by QtCreator 2015-11-22T21:29:43
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = heritage
TEMPLATE = app

DESTDIR = $$PWD

SOURCES += main.cpp\
        mainwindow.cpp \
    dombase.cpp \
    employee.cpp \
    product.cpp

HEADERS  += mainwindow.h \
    dombase.h \
    employee.h \
    product.h

FORMS    += mainwindow.ui
