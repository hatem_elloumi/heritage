#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "employee.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    Employee e;
    ui->labelNumber->setText(QString::number(e.getCount()));
    ui->tableViewEmployee->setModel(e.selectAll());
}

MainWindow::~MainWindow()
{
    delete ui;
}
