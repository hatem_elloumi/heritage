#include "dombase.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>


bool executeQuery(QSqlQuery& queryObj, QString& queryTxt) {
    queryObj.prepare(queryTxt);
    if (!queryObj.exec()) {
        qDebug("Error occurred while executing SQL.");
        qDebug("%s.", qPrintable(queryObj.lastError().text()));
        return false;
    }
    return true;
}

int DomBase::getCount() const {
    QString select =  "SELECT COUNT(*) FROM " + getTableName();
    QSqlQuery query;
    if (!executeQuery(query, select))
        return -1;

    query.next();
    qDebug("count is %d", query.value(0).toInt());
    return query.value(0).toInt();
}


QSqlQueryModel* DomBase::selectAll() const {
    QSqlQueryModel * model = new QSqlQueryModel();
    QString select =  "SELECT * FROM " + getTableName();
    QSqlQuery query;
    if (executeQuery(query, select))
        model->setQuery(query);
    return model;
}




